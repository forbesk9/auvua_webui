var gamepads = {};
var gamepadConnected = false;
var gamepadInterval;

window.addEventListener("gamepadconnected", function(e) {
    var gp = navigator.getGamepads()[e.gamepad.index];
    gamepads[gp.index] = gp;

    console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
        gp.index, gp.id,
        gp.buttons.length, gp.axes.length);

    if(!gamepadConnected) {
        gamepadConnected = true;
        gamepadInterval = setInterval(pollGamepads, 50);
    }
});

window.addEventListener("gamepaddisconnected", function(e) {
    delete gamepads[e.gamepad.index];

    console.log("Gamepad disconnected at index %d.", e.gamepad.index);
    console.log("Gamepads connected: %d", Object.keys(gamepads).length);

    if(Object.keys(gamepads).length < 1) {
        clearInterval(gamepadInterval);
        gamepadConnected = false;
    }
});

function getGamepadValues(index) {
    var result = "";

    var gp = navigator.getGamepads()[index];
    
    if(!gp) {
      return result;
    }

    for(var btn in gp.buttons) {
        result += "Button " + btn + ": " + gp.buttons[btn].value + "</br>";
    }

    for(var axis in gp.axes) {
        result += "Axis " + axis + ": " + gp.axes[axis] + "</br>";
    }

    return result;
}

function pollGamepads() {
    if(!gamepadConnected)
        return;

    for(var index in gamepads) {
        gamepads[index] = navigator.getGamepads()[index];
    }
};

if((navigator.getGamepads()).length > 0) {
    for(var x = 0; x < navigator.getGamepads().length; x++) {
        var gp = navigator.getGamepads()[x];

        if(gp != undefined) {
            gamepads[gp.index] = gp;
            console.log("Gamepad detected at index %d: %s. %d buttons, %d axes.",
                gp.index, gp.id,
                gp.buttons.length, gp.axes.length);

            if(!gamepadConnected) {
                gamepadConnected = true;
                gamepadInterval = setInterval(pollGamepads, 50);
            }
        }
    }
}
